import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Supermercado SS",
    },
    {
      id: 2,
      title: "Enviar e-mail",
      date: "2024-02-28",
      time: "14:30",
      address: "Trabalho",
    },
    {
      id: 3,
      title: "Estudar React Native",
      date: "2024-03-01",
      time: "09:00",
      address: "Casa",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", { task });
  };
  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
